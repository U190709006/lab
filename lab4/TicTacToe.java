import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
	Scanner reader = new Scanner(System.in);
	char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        int playtime=0;        
        int row;
        int col;
        boolean game= true;
	printBoard(board);
        while(game == true){
	    boolean player2check=true;			    
       	    System.out.print("Player 1 enter row number:");
	    row = reader.nextInt();
	    System.out.print("Player 1 enter column number:");
            col= reader.nextInt();              
            if((row > 3)||(col>3)){
                System.out.println("coordinates out of range");
                System.out.println("enter right coordinates!");
                continue;
                
                }            
            else if ((board[row - 1][col - 1])!= ' '){
                System.out.println("This space is occupied");
                System.out.println("enter right coordinates!");
                continue;
                
                }		    
            else{ 
                board[row - 1][col - 1] = 'X'; 		    
                printBoard(board);
                playtime+=1;
            }
            if (gamecheck1(board,row,col)==true||gamecheck2(board,row,col)==true){
                game=false;
                System.out.println("Player 1 wins");                
                continue;
	    }                            
            else if (playtime >= 9){
                System.out.println("GAME OVER");
                System.out.println("No Winner!");
                game = false;
                continue;                
            }	
            while(player2check==true){
		    System.out.print("Player 2 enter row number:");
		    row = reader.nextInt();
		    System.out.print("Player 2 enter column number:");
		    col = reader.nextInt();
		    if((row > 3)||(col>3)){
		        System.out.println("coordinates out of range");
		        System.out.println("enter right coordinates! ");
			printBoard(board);
			continue;
		        
		        }
		    else if ((board[row - 1][col - 1])!=' '){
		        System.out.println("This space is occupied");
		        System.out.println("enter right coordinates!");
			printBoard(board);
		        continue;
		        
		        }		                   
		    else{ 
		        board[row - 1][col - 1] = 'O';
		        playtime+=1;
			player2check=false;  		    
		        printBoard(board);
		        }
		     if (gamecheck1(board,row,col)==true||gamecheck2(board,row,col)==true){
		        game=false;
		        System.out.println("Player 2 wins");                
		        continue;
			}
			
            
		   }
        }
    
}
    public static boolean gamecheck1(char[][] board,int rowlast,int collast){
        char symbol=board[rowlast-1][collast-1];
        boolean win = true;
        for(int col = 0; col<3; col++) { 
	   if(board[rowlast-1][col] != symbol){
                win = false;
                break;
	    }	
	   
        }   	 	    
        if((board[0][0]==symbol)&&(board[1][1]==symbol)&&(board[2][2]==symbol)){           	
		win=true;		
		return win;
        }
        else if((board[0][2]==symbol)&&(board[1][1]==symbol)&&(board[2][0]==symbol)){           		
		win=true;		
		return win;
        	}
	return win;    
}
    public static boolean gamecheck2(char[][] board,int rowlast,int collast){
	char symbol=board[rowlast-1][collast-1];
	boolean win=true;
	for(int row = 0 ; row<3 ; row++) { 
	    if(board[row][collast-1] != symbol){
                win = false;
                break;
	    }    	 	    
	}
	return win;
}    
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
