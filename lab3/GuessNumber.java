import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		boolean flag = true;
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
        System.out.println("(Type -1 if you want to exit)");
        int attempt = 1;		
        do {
        int guess=reader.nextInt();
        
        if (guess == number){
        System.out.println("you win after "+attempt+" attempts");
        flag = false;
        }
        else if (guess == -1){
        System.out.println("the number was "+ number);
        flag = false;
        }
        else if ( guess < number){
        System.out.println("try bigger");
        attempt+=1;
        
        }
        else if( guess > number){
        System.out.println("try less");
        attempt+=1;
        }  
            }        
        while (flag == true);
            
		
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
