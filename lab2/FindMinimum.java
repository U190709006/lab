public class FindMinimum{
	public static void main(String[] args){
		int val1 = Integer.parseInt(args[0]);
		int val2 = Integer.parseInt(args[1]);
		int val3 = Integer.parseInt(args[2]);
		int result;
		
		if ((val1<val2)&&(val1<val3)){
			result = val1;
		}
		else if ((val2<val1)&&(val2<val3)){
			result = val2;
		}
		else{
			result = val3;
		}
		System.out.println(result);
	}
}	
