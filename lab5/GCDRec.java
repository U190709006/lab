public class GCDRec{
	public static void main(String[] args){
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		System.out.println(GCD(num1,num2));
	}
	
	public static int GCD(int x,int y){
		if (y == 0)
		    return x;
	        return GCD(y,x%y);
	}
}
